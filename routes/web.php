<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware( 'auth')->name('home');

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('admin');
Route::resource('brands','BrandController')->middleware('admin');
Route::resource('categories','CategoryController')->middleware('admin');
Route::resource('products','ProductController')->middleware('admin');
Route::post('import','ProductController@import')->name('import')->middleware('admin');
Route::post('brand.import','BrandController@import')->name('brand.import')->middleware('admin');

Route::post('category.import','CategoryController@import')->name('category.import')->middleware('admin');

Route::get('myanmar_idol', function (){
   return view('myanmaridol');
});

Route::get('image', 'GetImgaeController@getimage');


Route::get('rate', 'RangeController@index')->name('range.index');
Route::post('rate/edit', 'RangeController@edit')->name('range.edit');

Route::post('rate', 'RangeController@store')->name('range.store');




