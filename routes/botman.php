<?php

use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use App\Models\Product;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\Element;
use App\Conversations\BuyConversation;
use Illuminate\Support\Facades\Artisan;
use App\Events\CreatedPersistentMenu;
use App\Models\Category;
use App\Models\Brand;


$botman = resolve('botman');

$botman->hears('GET_STARTED', function ($bot) {
    $user = $bot->getUser();
    $bot->reply( 'မဂၤလာပါ။' .$user->getFirstName() .'! Home of Tone မွ ၾကိဳဆိုပါတယ္။ ဆိုင္လိပ္စာက အမွတ္ ၂၅ ကၽြန္းေတာေက်ာင္းလမ္း၊ စမ္းေခ်ာင္း။ ဆိုင္ဖြင့္ခ်ိန္ 9:30-5:30. Mon-Sat.Sunday ပိတ္ပါတယ္။');
});

$botman->hears('{userInput}', function ($bot, $userInput) {
//    event(new CreatedPersistentMenu());
//    Artisan::call('config:clear');
//    Artisan::call('botman:facebook:AddMenu');
    $productCategories = collect([]);
    $productBrands = collect([]);
    $products = collect([]);
    if (preg_match('/^[\w\s?]+$/si', $userInput)) {
        // input text is just English or Numeric or space
        $searches = preg_replace('/[^A-Za-z0-9\-]/', '', explode(" ", $userInput));
//        dd($searches);
    } else {
        $searches = str_replace(' ', '', iconv('UTF-8', 'ASCII//IGNORE', $userInput));
        $searches = preg_replace('/[^A-Za-z0-9\-]/', '', explode(" ", $searches));
    }
    foreach ($searches as $search) {
        if (!$search) {
            break;
        }
        $productBrands = Product::whereHas('brands', function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        })->take(9)->get();

        if ($productBrands->count() > 0) {
            break;
        }

        $productCategories = Product::whereHas('categories', function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        })->take(9)->get();

        if ($productCategories->count() > 0) {
            break;
        }

        $products = Product::where('name', $search)->orWhere('model', $search)->take(9)->get();
        if ($products->count() > 0) {
            break;
        }
    }


    if (($productCategories->count() > 0 || $productBrands->count() > 0 || $products->count() > 0) && substr($userInput, 0, 6) != 'search' && substr($userInput, 0, 4) != 'call') {
        //category list display
        if ($productCategories->count() > 0) {
            $elements = [];
            foreach ($productCategories as $productCategory) {
                $category = Element::create($productCategory->model)
                    ->subtitle($productCategory->name . PHP_EOL . $productCategory->price)
                    ->image('https://homeoftone.audio/image?name='.$productCategory->model)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload('ofni' . $productCategory->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
                $category_keyword = $productCategory->name;
            }
            if ($productCategories->count() < 9) {

            } else {
                $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                    ->image('https://homeoftone.audio/image?name=more')
                    ->addButton(ElementButton::create('View More')
                        ->payload('search me ' . $search)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            if ($productCategories->count() > 0) {
                $bot->reply(GenericTemplate::create()
                    ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                    ->addElements(
                        $elements
                    )
                );
            }
        }

        // Brand List Display
        if ($productBrands->count() > 0) {
            $elements = [];
            foreach ($productBrands as $productBrand) {
                $category = Element::create($productBrand->model)
                    ->subtitle($productBrand->name . PHP_EOL . $productBrand->price)
                    ->image('https://homeoftone.audio/image?name='.$productBrand->model)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload('ofni' . $productBrand->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
                $brand_keyword = $productBrand->name;
            }
            if ($productBrands->count() < 9) {

            } else {
                $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                    ->image('https://homeoftone.audio/image?name=more')
                    ->addButton(ElementButton::create('View More')
                        ->payload('search me ' . $search)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            if ($productBrands->count() > 0) {
                $bot->reply(GenericTemplate::create()
                    ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                    ->addElements(
                        $elements
                    )
                );
            }
        }

        //product exact match
        if ($products->count() > 0) {
            $elements = [];
            foreach ($products as $product) {
                $category = Element::create($product->model)
                    ->subtitle($product->name . PHP_EOL . $product->price)
                    ->image('https://homeoftone.audio/image?name='.$product->model)
                    ->addButton(ElementButton::create('Buy Now')
                        ->payload('syub' . $product->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            if ($products->count() > 0) {
                $bot->reply(GenericTemplate::create()
                    ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                    ->addElements(
                        $elements
                    )
                );
            }
        }
    } else {
        $userinput = substr($userInput, 0, 4);
        if ($userinput == 'syub' || $userinput == 'ofni' || $userInput === 'Get Started' || $userInput == 'Category' || $userInput == 'Brand' || $userInput === 'View More' || substr($userInput, 0, 6) == 'search'
            || substr($userInput, 0, 4) == 'call' ) {
        } else {
            $bot->reply(ButtonTemplate::create('Do you want to know more about Home of Tone?')
                ->addButton(ElementButton::create('Categories')
                    ->type('postback')
                    ->payload('Category')
                )
                ->addButton(ElementButton::create('Brands')
                    ->type('postback')
                    ->payload('Brand')
                )
                ->addButton(ElementButton::create('Call Admin')
                    ->type('phone_number')
                    ->payload('+959973761642')
                )
            );
        }
    }
});

$botman->hears('{peristent}', function ($bot, $persistent) {

    if ($persistent == 'Category') {
        $categories = Category::take(9)->get();
        if ($categories->count() > 0) {
            $elements = [];
            foreach ($categories as $category) {
                $category = Element::create($category->name)
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                    ->image('https://homeoftone.audio/image?name='.$category->name)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload($category->name)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }

            $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                ->image('https://homeoftone.audio/image?name=more')
                ->addButton(ElementButton::create('View More')
                    ->payload('call me persistentcategory')
                    ->type('postback')
                );
            array_push($elements, $category);
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements(
                    $elements
                )
            );
        }

    } elseif ($persistent == 'Brand') {
        $brands = Brand::take(9)->get();
        if ($brands->count() > 0) {
            $elements = [];
            foreach ($brands as $brand) {
                $category = Element::create($brand->name)
//                    ->subtitle($brand->name.PHP_EOL.$productCategory->price)
                    ->image('https://homeoftone.audio/image?name='.$brand->name)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload($brand->name)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                ->image('https://homeoftone.audio/image?name=more')
                ->addButton(ElementButton::create('View More')
                    ->payload('call me persistentbrand')
                    ->type('postback')
                );
            array_push($elements, $category);
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements(
                    $elements
                )
            );
        }

    }

});

$botman->hears('call me {type}', function ($bot, $type) {
    $user = $bot->getUser();
    $id = $user->getId();
    $userinformation = $bot->userStorage()->find($id);
    $usercount = $userinformation->get('count');
    $usertype = $userinformation->get('type');

    if ($type == $usertype) {
        $bot->userStorage()->save([
            'id' => $id,
            'count' => $usercount + 9,
            'type' => $usertype
        ]);
    } else {
        $bot->userStorage()->save([
            'id' => $id,
            'count' => 9,
            'type' => $type
        ]);
    }

    $userinformation = $bot->userStorage()->find($id);
    $count = $userinformation->get('count');
    $usertype = $userinformation->get('type');

    if ($usertype == 'persistentcategory') {
        $brands = Category::get();
        $brands = $brands->slice($count, 9);
        $brands->all();
        if ($brands->count() > 0) {
            $elements = [];
            foreach ($brands as $brand) {
                if ($brand->name != 'Nth') {
                    $category = Element::create($brand->name)
//                    ->subtitle($brand->name.PHP_EOL.$productCategory->price)
                        ->image('https://homeoftone.audio/image?name='.$brand->name)
                        ->addButton(ElementButton::create('Detailed')
                            ->payload($brand->name)
                            ->type('postback')
                        );
                    array_push($elements, $category);
                }
            }
            if ($brands->count() < 9) {
            } else {
                $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                    ->image('https://homeoftone.audio/image?name=more')
                    ->addButton(ElementButton::create('View More')
                        ->payload('call me persistentcategory')
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements(
                    $elements
                )
            );
        }
    } elseif ($usertype == 'persistentbrand') {
        $user_brands = Brand::get();
        $user_brands = $user_brands->slice($count, 9);
        $user_brands->all();
        if ($user_brands->count() > 0) {
            $elements = [];
            foreach ($user_brands as $brand) {
                if ($brand->name != 'nth') {
                    $category = Element::create($brand->name)
//                    ->subtitle($brand->name.PHP_EOL.$productCategory->price)
                        ->image('https://homeoftone.audio/image?name='.$brand->name)
                        ->addButton(ElementButton::create('Detailed')
                            ->payload($brand->name)
                            ->type('postback')
                        );
                    array_push($elements, $category);
                }
            }
            if ($user_brands->count() < 9) {

            } else {
                $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
                    ->image('https://homeoftone.audio/image?name=more')
                    ->addButton(ElementButton::create('View More')
                        ->payload('call me persistentbrand')
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements(
                    $elements
                )
            );
        }
    }

});

$botman->hears('search me {type}', function ($bot, $type) {
    $user = $bot->getUser();
    $id = $user->getId();
    $userinformation = $bot->userStorage()->find($id);
    $usercount = $userinformation->get('count');
    $usertype = $userinformation->get('type');

    if ($type == $usertype) {
        $bot->userStorage()->save([
            'id' => $id,
            'count' => $usercount + 9,
            'type' => $usertype
        ]);
    } else {
        $bot->userStorage()->save([
            'id' => $id,
            'count' => 9,
            'type' => $type
        ]);
    }

    $userinformation = $bot->userStorage()->find($id);
    $count = $userinformation->get('count');
    $usertype = $userinformation->get('type');

    $products = Product::where('name', $usertype)->orWhere('model', $usertype)->get();

//    $productBrands = Product::whereHas('brands', function ($query) use ($usertype) {
//        $query->where('name', 'like', '%' . $usertype . '%');
//    })->get();

    $products = $products->slice($count, 9);
    $products->all();
    $elements = [];


    if ($products->count() > 0) {
        $elements = [];
        foreach ($products as $product) {
            if ($product->name != 'nth') {
                $category = Element::create($product->model)
                    ->subtitle($product->name . PHP_EOL . $product->price)
                    ->image('https://homeoftone.audio/image?name='.$product->model)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload('ofni' . $product->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $product_search = $product->name;

        }
    }
    if ($products->count() < 9) {

    } else {
        $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
            ->image('https://homeoftone.audio/image?name=more')
            ->addButton(ElementButton::create('View More')
                ->payload('search me ' . $usertype)
                ->type('postback')
            );
        array_push($elements, $category);
    }
    if ($products->count() > 0) {
        $bot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElements(
                $elements
            )
        );
    }
    $productBrands = Product::whereHas('brands', function ($query) use ($usertype) {
        $query->where('name', 'like', '%' . $usertype . '%');
    })->get();

    $productBrands = $productBrands->slice($count, 9);
    $productBrands->all();
    $elements = [];


    if ($productBrands->count() > 0) {
        $elements = [];
        foreach ($productBrands as $brand) {
            if ($brand->name != 'nth') {
                $category = Element::create($brand->model)
                    ->subtitle($brand->name . PHP_EOL . $brand->price)
                    ->image('https://homeoftone.audio/image?name='.$brand->model)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload('ofni' . $brand->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $productBrand_search = $brand->name;

        }
    }
    if ($productBrands->count() < 9) {

    } else {
        $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
            ->image('https://homeoftone.audio/image?name=more')
            ->addButton(ElementButton::create('View More')
                ->payload('search me ' . $usertype)
                ->type('postback')
            );
        array_push($elements, $category);
    }
    if ($productBrands->count() > 0) {
        $bot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElements(
                $elements
            )
        );
    }

    $productCategories = Product::whereHas('categories', function ($query) use ($usertype) {
        $query->where('name', 'like', '%' . $usertype . '%');
    })->get();

    $productCategories = $productCategories->slice($count, 9);
    $productCategories->all();

    $elements = [];
    if ($productCategories->count() > 0) {
        $elements = [];
        foreach ($productCategories as $productCategory) {
            if ($productCategory->name != 'nth') {
                $category = Element::create($productCategory->model)
                    ->subtitle($productCategory->name . PHP_EOL . $productCategory->price)
                    ->image('https://homeoftone.audio/image?name='.$productCategory->model)
                    ->addButton(ElementButton::create('Detailed')
                        ->payload('ofni' . $productCategory->id)
                        ->type('postback')
                    );
                array_push($elements, $category);
            }
            $productCategory_search = $productCategory->name;

        }
    }
    if ($productCategories->count() < 9) {

    } else {
        $category = Element::create('More')
//                    ->subtitle($category->name.PHP_EOL.$category->price)
            ->image('https://homeoftone.audio/image?')
            ->addButton(ElementButton::create('View More')
                ->payload('search me ' . $usertype)
                ->type('postback')
            );
        array_push($elements, $category);
    }
    if ($productCategories->count() > 0) {
        $bot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElements(
                $elements
            )
        );
    }

});


$botman->hears('{second_input}', function ($bot, $second_input) {
    $keyword = substr($second_input, 0, 4);
    $id = substr($second_input, 4, 60);
    $product_info = Product::where('id', $id)->first();
    if ($keyword == 'syub') {
        $bot->startConversation(new BuyConversation($product_info->model));
    } elseif ($keyword == 'ofni') {
        $bot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElements([
                Element::create($product_info->model)
                    ->subtitle($product_info->name . PHP_EOL . $product_info->price)
                    ->image('https://homeoftone.audio/image?name='.$product_info->model)
                    ->addButton(ElementButton::create('Buy Now')
                        ->payload('syub' . $product_info->id)
                        ->type('postback')
                    )
            ])
        );
    }

});