@extends('layouts.backend')

@section('content')

<div class="container">
     <div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
             <h3><b>Rate</b></h3>
             <br>

<form action="{{ route('range.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Today Rate</label>
                            <input id="title" type="text" name="range" class="form-control" value="{{ Setting::get( 'range' ) }}" autofocus  />
                        </div>
                       
                      </div>
                      
                        
                    </div>
<br>
                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Update">
                      
                    </div>
                </div>
            </form>    </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection