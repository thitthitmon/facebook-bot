@extends('layouts.backend')

@section('content')

<div class="container">
	 <div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
        	 <h3><b>Add New Category</b></h3>
        	 <br>

<form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Category Name</label>
                            <input id="title" type="text" name="category_name" class="form-control" value="{{ old('category_name') }}" autofocus  />
                        </div>
                        <div class="uk-form-row">
                            <label for="title">Category Priority</label>
                            <input id="title" type="text" name="csort" class="form-control" value="{{ old('csort') }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
<br>
                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Create">
                      
                    </div>
                </div>
            </form>    </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection