@extends('layouts.backend')

@section('content')

 <div class="content">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                {{--<a  class="btn btn-info" href="{!! route('brands.index') !!}" >Back</a>--}}
                <a  class="btn btn-success" href="{!! route('categories.create') !!}" >Create</a>
            </div>
                    @if (session('categories'))
                        <div class="alert alert-success">
                            {{ session('categories') }}
                        </div>
                    @endif
                     @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
                    @endif
                     @if (session('delete'))
                        <div class="alert alert-success">
                            {{ session('delete') }}
                        </div>
                    @endif
          <div class="row">
            <div class="col-md-12">
               <form action="{{ route('category.import') }}" method="post" enctype="multipart/form-data" >
                @csrf
               <div class="row">
                 <div class="col-md-3"><input type="file" name="file" class="form-control" required></div>
                 <div class="col-md-2"><button type="submit" class="btn btn-primary pull-right">Import Excel
                  </button> 
                 </div>
               </div>
             </form>

              <div class="card">
               
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Categories</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                           <table id="myTable" class="display">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Name
                        </th>
                        <th>
                          Priority
                        </th>
                        <th>
                         Action
                        </th>
                        
                      </thead>
                      <tbody>
                      	@foreach ($categories  as $category)
                        <tr>
                        	<td>
                            {{$category->id}}
                          </td>
                          <td>
                            {{$category->name}}
                          </td>
                          <td>
                            {{$category->csort}}
                          </td>
                          <td>
                             <div>
                            {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                            <a href="{!! route('categories.show', [$category->id]) !!}"><i class="material-icons">
                                  visibility
                                  </i></a>
                              <a href="{!! route('categories.edit', [$category->id]) !!}"><i class="material-icons">
                                  create
                                  </i></a>


                            {!! Form::button('<i class="material-icons">
                              delete_outline
                              </i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure? Products of this category will automatically delete!')"]) !!}
                              {!! Form::close() !!}
                          </td>

                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="card card-plain">
               
               
              </div>
            </div>
          </div>
        </div>
      </div>


@endsection