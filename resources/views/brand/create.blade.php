@extends('layouts.backend')

@section('content')

<div class="container">
	 <div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
        	 <h3><b>Add New Brand</b></h3>
        	 <br>

<form action="{{ route('brands.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Brand Name</label>
                            <input id="title" type="text" name="brand_name" class="form-control" value="{{ old('brand_name') }}" autofocus  />
                        </div>
                        <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Brand Priority</label>
                            <input id="title" type="text" name="sort" class="form-control" value="{{ old('sort') }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        
                      </div>
                      
                        
                    </div>
<br>
                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Create">
                      
                    </div>
                </div>
            </form>    </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection