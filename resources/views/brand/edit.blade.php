@extends('layouts.backend')

@section('content')

<div class="container">
	<div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
        	 <h3><b>Edit  Brand</b></h3>
        	 <br>



{!! Form::model($brands, ['route' => ['brands.update', $brands->id], 'method' => 'patch','files' => true]) !!}

<div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Brand Name</label>
                            <input id="title" type="text" name="brand_name" class="form-control" value="{{ $brands->name }}" autofocus  />
                        </div>
                        <div class="uk-form-row">
                            <label for="title">Brand Priority</label>
                            <input id="title" type="text" name="sort" class="form-control" value="{{ $brands->sort }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
<br>
                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Edit">
                      
                    </div>
                </div>
            {!! Form::close() !!}
    </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection