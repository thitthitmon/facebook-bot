@extends('layouts.backend')

@section('content')


 <div class="content">

        <div class="container-fluid">
            <div class="navbar-wrapper">
                {{--<a  class="btn btn-info" href="{!! route('brands.index') !!}" >Back</a>--}}
                <a  class="btn btn-success" href="{!! route('brands.create') !!}" >Create</a>
            </div>
           @if (session('brands'))
                        <div class="alert alert-success">
                            {{ session('brands') }}
                        </div>
            @endif
            @if (session('update'))
                        <div class="alert alert-success">
                            {{ session('update') }}
                        </div>
            @endif
            @if (session('delete'))
                        <div class="alert alert-success">
                            {{ session('delete') }}
                        </div>
            @endif
          <div class="row">
            <div class="col-md-12">
               <form action="{{ route('brand.import') }}" method="post" enctype="multipart/form-data">
                @csrf
              <div class="col-md-12">               
                <div class="row">
                  <div class="col-md-3"><input type="file" name="file" class="form-control" required ></div>
                  <div class="col-md-2"><button type="submit" class="btn btn-primary pull-right">Import Excel
                    </button> 
                </div>
              </div>
            </form>
              <div class="card">
                
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Brand Name</h4>
                </div>
                <div class="card-body">
                           <table id="myTable" class="display">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Name
                        </th>
                        <th>
                          Priority
                        </th>
                        <th>
                         Action
                        </th>
                        
                      </thead>
                      <tbody>
                      	@foreach ($brands  as $brand)
                        <tr>
                        	<td>
                            {{$brand->id}}
                          </td>
                          <td>
                            {{$brand->name}}
                          </td>
                           <td>
                            {{$brand->sort}}
                          </td>
                          <td>
                            {!! Form::open(['route' => ['brands.destroy', $brand->id], 'method' => 'delete']) !!}
                            <a href="{!! route('brands.show', [$brand->id]) !!}"><i class="material-icons">
                                  visibility
                                  </i></a>
                              <a href="{!! route('brands.edit', [$brand->id]) !!}"><i class="material-icons">
                                  create
                                  </i></a>
                            

                            {!! Form::button('<i class="material-icons">
                              delete_outline
                              </i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                              {!! Form::close() !!}
                          </td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="card card-plain">
               
               
              </div>
            </div>
          </div>
        </div>
      </div>



@endsection