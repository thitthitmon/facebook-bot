@extends('layouts.backend')

@section('content')
<div class="container">
	 <div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
        	 <h3><b>Add New Product</b></h3>
        	 <br>

<form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Name</label>
                            <input id="title" type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Model</label>
                            <input id="title" type="text" name="model" class="form-control" value="{{ old('model') }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="uk-form-row">
                                        <label for="title">Price in MMK</label>
                                        <input id="title" type="text" name="price_mmk" class="form-control" value="{{ old('price_mmk') }}" autofocus  />
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                     <div class="uk-form-row">
                                            <label for="title"> Price in USD </label>
                                            <input id="title" type="text" name="price_usd" class="form-control" value="{{ old('price_usd') }}" autofocus  />
                                        </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="uk-form-row">
                                        <label for="title"> Qty</label>
                                        <input id="title" type="text" name="qty" class="form-control" value="{{ old('price') }}" autofocus  />
                                    </div>
                                </div>
                            </div>
                        
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Description</label>
                            <input id="title" type="textarea" name="description" class="form-control" value="{{ old('description') }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                    <div class="uk-form-row">
                            <label for="title">Select Category</label>
                            <select name="category_name" class="form-control">                          
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">
                                {{$category->name}} 
                                </option>
                                @endforeach
                            </select>
                           
                        </div>
                        <div class="uk-form-row">
                            <label for="title">Select Brand</label>
                            <select name="brand_name" class="form-control">                          
                                @foreach($brands as $brand)
                                <option value="{{$brand->id}}">
                                {{$brand->name}} 
                                </option>
                                @endforeach
                            </select>
                           
                        </div>
<br>
                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Create">
                      
                    </div>
                </div>
            </form>    </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection
