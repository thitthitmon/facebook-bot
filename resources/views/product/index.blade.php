@extends('layouts.backend')

@section('content')




 <div class="content">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                {{--<a  class="btn btn-info" href="{!! route('brands.index') !!}" >Back</a>--}}
                <a  class="btn btn-success" href="{!! route('products.create') !!}" >Create</a>
            </div>
          @if (session('products'))
                        <div class="alert alert-success">
                            {{ session('products') }}
                        </div>
                    @endif
              @if (session('update'))
                  <div class="alert alert-success">
                      {{ session('update') }}
                  </div>
              @endif
          <div class="row">
  <div class="col-md-12">

 <form action="{{ route('import') }}" method="post" enctype="multipart/form-data" >
                @csrf
               <div class="row">
                 <div class="col-md-3"><input type="file" name="file" class="form-control" required></div>
                 <div class="col-md-2"><button type="submit" class="btn btn-primary pull-right">Import Excel
                  </button> 
                 </div>
               </div>
             </form>
              <div class="card">
                <div class="card-header card-header-primary">
                  
                  <h4 class="card-title ">Product Name</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                           <table id="myTable" class="display">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Name
                        </th>
                         <th>
                          Model
                        </th>
                         <th>
                          Price in MMK
                        </th>
                         <th>
                          Price in USD
                        </th>
                         <th>
                          QTY
                        </th>
                         <th>
                          Description
                        </th>
                        <th>
                         Category
                        </th>
                        <th>
                         Brand
                        </th>
                        <th>
                         Action
                        </th>
                        
                      </thead>
                      <tbody>


                        @foreach ($products  as $product)
                        <tr>
                        	<td>
                            {{$product->id}}
                          </td>
                          <td>
                          {{$product->name}}
                          </td>
                           <td>
                           {{$product->model}}
                          </td>
                           <td>
                           {{$product->price_mmk}}
                          </td>
                           <td>
                           {{$product->price_usd}}
                          </td>
                           <td>
                           {{$product->qty}}
                          </td>
                           <td>
                           {{$product->description}}
                          </td>
                           <td>
                           {{$product->categories['name']}}
                          </td>
                           <td>
                           {{$product->brands['name']}}
                          </td>


                          <td>
                            {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                            <a href="{!! route('products.show', [$product->id]) !!}"><i class="material-icons">
                                  visibility
                                  </i></a>
                              <a href="{!! route('products.edit', [$product->id]) !!}"><i class="material-icons">
                                  create
                                  </i></a>
                            
                            {!! Form::button('<i class="material-icons">
                              delete_outline
                              </i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                          </td>
                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="card card-plain">
               
               
              </div>
            </div>
          </div>
        </div>
      </div>


@endsection
