@extends('layouts.backend')

@section('content')
    <div class="container">

        <br>
        <br>
        <h3><b>View Brand</b></h3>
        <br>



            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Brand</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->categories->name }}</td>
                    <td>{{ $product->brands->name }}</td>
                </tr>
                </tbody>
            </table>

        </div>


@endsection