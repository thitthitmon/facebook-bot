@extends('layouts.backend')

@section('content')
<div class="container">
	 <div class="row">
    <div class="col-md-3">
     
    </div>
    <div class="col-md-6">
    
<br>
<br>
        	 <h3><b>Edit Product</b></h3>
        	 <br>

{!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'patch','files' => true]) !!}
                {{ csrf_field() }}
                
                <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Name</label>
                            <input id="title" type="text" name="name" class="form-control" value="{{ $product->name }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Model</label>
                            <input id="title" type="text" name="model" class="form-control" value="{{ $product->model }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="uk-form-row">
                                        <label for="title">Product Price in MMK</label>
                                        <input id="title" type="text" name="price_mmk" class="form-control" value="{{ $product->price_mmk}}" autofocus  />
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                     <div class="uk-form-row">
                                            <label for="title"> Product Price in USD </label>
                                            <input id="title" type="text" name="price_usd" class="form-control" value="{{ $product->price_usd }}" autofocus  />
                                        </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="uk-form-row">
                                        <label for="title"> Qty</label>
                                        <input id="title" type="text" name="qty" class="form-control" value="{{ $product->qty }}" autofocus  />
                                    </div>
                                </div>
                            </div>
                        
                      </div>
                      
                        
                    </div>
                     <div class="uk-width-medium-1-2">
                    <div class="uk-form-row">
                        <div class="uk-form-row">
                            <label for="title">Product Description</label>
                            <input id="title" type="textarea" name="description" class="form-control" value="{{ $product->description }}" autofocus  />
                        </div>
                      </div>
                      
                        
                    </div>
                    <div class="uk-form-row">
                            <label for="title">Select Category</label>
                        {!! Form::select('category_name', $categories->pluck('name','id'), $product->category_id, ['class'=>'form-control']) !!}
                        </div>
                        <div class="uk-form-row">
                            <label for="title">Select Brand</label>

                            {!!  Form::select('brand_name', $brands->pluck('name','id'), $product->brand_id, ['class'=>'form-control']) !!}
                           
                        </div>
<br>

                    <div class="uk-form-row">
                       <input type="submit" class="btn btn-success" value="Update">
                      
                    </div>
                </div>
                        {!! Form::close() !!}
  </div>
    <div class="col-md-3">
    
    </div>
  </div>
</div>

@endsection
