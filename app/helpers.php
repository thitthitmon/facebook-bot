<?php
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Support\Facades\DB;



if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('access')) {
    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function access()
    {
        return app('access');
    }
}

if (! function_exists('history')) {
    /**
     * Access the history facade anywhere.
     */
    function history()
    {
        return app('history');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('brand')) {
    /**
     * Access the gravatar helper.
     */
    function brand()
    {
        $myfile = fopen(storage_path('app/brand.txt'), "r") or die("Unable to open file!");
        $data = fread($myfile,filesize(storage_path('app/brand.txt')));
        $brands = json_decode($data);
                $call_to_action_brand = [];

        foreach ($brands as $brand)
        {
            array_push($call_to_action_brand,    [
                'title' => $brand->name,
                'type' => 'postback',
                'payload' => $brand->name,
            ]);
        }
        return $call_to_action_brand;
    }
}

if (! function_exists('category')) {
    /**
     * Access the gravatar helper.
     */
    function category()
    {
        $myfile = fopen(storage_path('app/category.txt'), "r") or die("Unable to open file!");
        $data = fread($myfile,filesize(storage_path('app/category.txt')));
        $categories = json_decode($data);
        $call_to_action_category = [];

        foreach ($categories as $category)
        {
            array_push($call_to_action_category,    [
                'title' => $category->name,
                'type' => 'postback',
                'payload' => $category->name,
            ]);
        }
        return $call_to_action_category;
    }
}




