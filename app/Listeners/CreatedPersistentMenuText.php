<?php

namespace App\Listeners;

use App\Events\CreatedPersistentMenu;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class CreatedPersistentMenuText
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedPersistentMenu  $event
     * @return void
     */
    public function handle(CreatedPersistentMenu $event)
    {
        $call_to_action_brand = [];
        $brands = Brand::all();
        foreach ($brands as $brand)
        {
            array_push($call_to_action_brand,    [
                'title' => $brand->name,
                'type' => 'postback',
                'payload' => $brand->name,
            ]);
        }

        $call_to_action_category = [];
        $categories = Category::all();
        foreach ($categories as $category)
        {
            array_push($call_to_action_category,    [
                'title' => $category->name,
                'type' => 'postback',
                'payload' => $category->name,
            ]);
        }

//
        Storage::put('brand.txt', $brands);
        Storage::put('category.txt', $categories);

        Artisan::call('config:clear');
        Artisan::call('botman:facebook:AddMenu');
    }
}
