<?php

namespace App\Http\Controllers;

use App\Events\CreatedPersistentMenu;
use Illuminate\Http\Request;
use App\Models\Brand;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function configu()
    {
//     $data = \Illuminate\Support\Facades\Storage::disk('local')->get('brand.txt');
        return 12321;
    }

    public  function __invoke()
    {
        event(new CreatedPersistentMenu());
    }

}
