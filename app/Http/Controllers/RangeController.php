<?php

namespace App\Http\Controllers;
use anlutro\LaravelSettings\SettingStore;
use Illuminate\Http\Request;

class RangeController extends Controller
{
     public function index()
    {
        return view('range.index');
    }

    public function edit( Request $request)
    {
       $range=$request['range'];
       
        \Setting::set("range" , $range);
        
        \Setting::save();         
        
        return redirect()->route('range.index')->with([   
            'flash_message' => 'Successfully updated Range .'     ]);

    }
public function store(Request $request)
    {

        $range=$request['range'];       

        foreach ($request->except(['_token']) as $key => $value){
            \Setting::set($key , $value);
            \Setting::save();
        }

        return view('range.update',compact('range'));

        
    }
}
