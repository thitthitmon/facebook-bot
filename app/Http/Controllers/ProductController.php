<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use DB;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $categories=Category::get();
        $brands=Brand::get();
        $products=Product::with('categories','brands')->get();

        return view('product.index', ['products' => $products]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::get();
        $brands=Brand::get();
        return view('product.create',compact('categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $product =new Product();
        $product->name=$request->input('product_name');
        $product->model=$request->input('model');
        $product->price_mmk=$request->input('price_mmk');
        $product->price_usd=$request->input('price_usd');
        $product->qty=$request->input('qty');
        $product->description=$request->input('description');
        $product->category_id=$request->input('category_name');
        $product->brand_id=$request->input('brand_name');
        $product->save();
        return redirect("products");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $categories=Category::get();
        $brands=Brand::get();
        $product=Product::with('categories','brands')
        ->where('id',$id)
        ->first();
        return view('product.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
        $product=Product::where('id',$id)->first();
        $categories=Category::all();
        $brands=Brand::all();


        return view('product.edit',compact('brands','categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $data = [
            'name'       => $request->get('name'),
            'model'       => $request->get('model'),
            'price_mmk'=> $request->get('price_mmk'),
            'price_usd'=> $request->get('price_usd'),
            'qty'      => $request->get('qty'),
            'description'      => $request->get('description'),
            'category_id'  => $request->get('category_name'),
            'brand_id'  => $request->get('brand_name')

        ];

        $product = Product::find($id);
        $product->price_mmk = $request->price_mmk;
       $product->price_usd = $request->price_usd;
        $product->update($data);
        $product->save();
        return redirect('products')->with('update','Product is successfully Updated');
    }

    public function import(Request $request)
    {
      $file = $request->file('file');      
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $tempPath = $file->getRealPath();
      $fileSize = $file->getSize();
      $mimeType = $file->getMimeType();
      $location = 'uploads';
      $file->move($location,$filename);
      $filepath = public_path($location."/".$filename);
      $file = fopen($filepath,"r");
      $importData_arr = array();
      $i = 0;
        

while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
             $num = count($filedata );
             
             // Skip first row (Remove below comment if you want to skip the first row)
             if($i == 0){
                $i++;
                continue; 
             }
             for ($c=0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata [$c];
             }
             $i++;
          }
          fclose($file);



           foreach($importData_arr as $importData){

                if($importData[6]=='-' || $importData[6] == '')
                {
                  $brand= DB::table('brands')
                           ->select('id')
                           ->where('name','nth')
                           ->first();
                $brand_id=$brand->id;
                }
                else  {
                    $brand= DB::table('brands')
                           ->select('id')
                           ->where('name',$importData[6])
                           ->first();
                    if(!isset($brand))
                    {
                       return response()->json(['statust'=> $importData[6]. ' does not exit in Brand List']);
                    }
                $brand_id=$brand->id;
                }

                if($importData[7]=='-' || $importData[7] == '')
                {
                $category= DB::table('categories')
                           ->select('id')
                           ->where('name','Nth')
                           ->first();
                $category_id=$category->id;
                }
                else {
                    $category= DB::table('categories')
                           ->select('id')
                           ->where('name',$importData[7])
                           ->first();
                    if(!isset($category))
                    {
                        return response()->json(['statust'=> $importData[7]. ' does not exit in Category List']);
                    }
                    $category_id=$category->id;
                }

                $product=new Product();
                $product->name=$importData[1];
                $product->model=$importData[0];
                $product->price_mmk=$importData[2];
                $product->price_usd=$importData[3];
                $product->qty=$importData[4];
                $product->description=$importData[5];
                $product->category_id=$category_id;
                $product->brand_id=$brand_id;
                $product->save();

            // $insertData = array(
            //    "name"=>$importData[1],
            //    "model"=>$importData[0],
            //    "price_mmk"=>$importData[2],
            //     "price_usd"=>$importData[3],
            //      "qty"=>$importData[4],
            //    "description"=>$importData[5],
            //    "category_id"=>$category_id,
            //     "brand_id"=>$brand_id);
            // Product::create($insertData);

          }
         

    // Redirect to index
        return redirect("products")->with('products','Products are successfully imported');
  
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
              $product->delete();
                return redirect('products')->with('delete','Product is successfully Deleted');
    }
}
