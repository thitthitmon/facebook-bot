<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GetImgaeController extends Controller
{
    public function getimage(Request $request)
    {
        $string = shell_exec('find '.public_path() . '/homeoftoneadmin/ftp/images/ -iname '.$request->name.'*');
        $lenght = strpos($string, '.');
        $extension = substr($string,$lenght,'6');
        $extension = str_replace("\n","",$extension);
        $path = public_path() . '/homeoftoneadmin/ftp/images' . '/' . $request->name.$extension;
        return response()->file($path);
    }
}
