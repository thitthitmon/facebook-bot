<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function brands(){
        return $this->belongsToMany('App\Models\Brand','category_brand');
    }
}
