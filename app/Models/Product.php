<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','model','price','range','qty','description','category_id','brand_id'];
    public function categories()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }

    public function brands()
    {
        return $this->belongsTo('App\Models\Brand','brand_id');
    }
}



