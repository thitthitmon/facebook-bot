<?php

namespace App\Conversations;


use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Illuminate\Support\Facades\Mail;

class BuyConversation extends Conversation
{

    protected $firstname;
    protected $name;
    protected $address;
    protected $phone;
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function askFirstname()
    {
        $this->ask('Hello! What is your name?', function(Answer $answer) {
            // Save result
            $this->firstname = $answer->getText();
            $this->askPhonenumber();
        });
    }

    public function askPhonenumber()
    {
        $this->ask('One more thing - what is your phone number?', function(Answer $answer) {
            // Save result
            $this->phone = $answer->getText();

//            $this->say('Great - that is all we need, '.$this->firstname);
            $this->askAddress();
        });
    }

    public function askAddress()
    {
        $this->ask('Last - what is your  address?', function(Answer $answer) {
            // Save result
            $this->address = $answer->getText();

            $this->say('Great - that is all we need, '.$this->firstname);
            $obj = new \stdClass();

            $obj->model = $this->model;
            $obj->phone = $this->phone;
            $obj->name = $this->firstname;
            $obj->address = $this->address;
            $obj->message = '';

            Mail::to('peter.tzht@gmail.com')->send(new \App\Mail\BuyNowSendEmail($obj));

        });
    }

    public function run()
    {
        // This will be called immediately
        $this->askFirstname();
    }

}
